#!/bin/bash

RSSRADIOXSLT=/usr/local/share/radio/rssradio.xslt

[ -r /etc/default/radio ] && source /etc/default/radio

echo -e "Content-Type: text/html; charset=UTF-8\n\n"

echo "<html><head><title>Radio podcast</title></head><body>"
if [ -x $(which curl) -a -x $(which xmlstarlet) -a -r ${RSSRADIOXSLT} -a ${#PODCASTADDRESS[@]} -gt 0 ]
then
    for (( X=1; ${#PODCASTADDRESS[@]}-X; X++ ))
    do
	curl --location -s ${PODCASTADDRESS[X]} | xmlstarlet tr ${RSSRADIOXSLT}
    done
fi
echo "</body></html>"
