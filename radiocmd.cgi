#!/bin/bash

# Pi radio CGI command.

if [ -v REQUEST_METHOD ] 
then
    echo -e "Content-Type: text/plain\n\n"

    if [ "${REQUEST_METHOD:0:3}" == "GET" ]
    then
        VAR=${HTTP_REFERER/#*\/radio\//}
        env PIPE=/srv/radio/fifo.${VAR/%\/*/} radiocmd "${QUERY_STRING}" 
    fi
fi

