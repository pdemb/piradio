## Pi radio makefile

SRVDIR   := /srv/radio
SBINDIR  := /usr/local/sbin
BINDIR   := /usr/local/bin
WEBDIR   := /var/www/radio
CGIDIR   := /usr/local/lib/cgi-bin
ETCDIR   := /etc/radio
SHAREDIR := /usr/local/share/radio

# the user you run it with has to have access to Pi's audio system
RADIOUSR := pi
RADIOGRP := pi

.PHONY: install

INSTALLCMD := sudo install -o $(RADIOUSR) -g $(RADIOGRP)

RADIOFILES := omx.keys radio radiocmd radiosrv \
  index.html radiocmd.cgi radiopodcast.cgi \
  rssradio.xslt

VARIANT ?=

install: radio$(VARIANT).service $(RADIOFILES)
	$(INSTALLCMD) -m 0755 -d $(SRVDIR)
	$(INSTALLCMD) -m 0755 -d $(ETCDIR)
	$(INSTALLCMD) -m 0755 -d $(SHAREDIR)
	$(INSTALLCMD) -m 0644 omx.keys $(ETCDIR)
	$(INSTALLCMD) -m 0644 radio /etc/default
	$(INSTALLCMD) -m 0644 rssradio.xslt $(SHAREDIR)

	$(INSTALLCMD) -m 0755 radiocmd $(BINDIR)
	$(INSTALLCMD) -m 0755 radiocmd $(BINDIR)/radiocmd$(if $(VARIANT),.$(VARIANT))
	$(INSTALLCMD) -m 0755 radiosrv $(SBINDIR)

#	web part, it works with any http server supporting CGI
	$(INSTALLCMD) -m 0755 -d $(WEBDIR)/$(VARIANT)/
	$(INSTALLCMD) -m 0644 index.html $(WEBDIR)/$(VARIANT)/
	$(INSTALLCMD) -m 0755 radiopodcast.cgi $(WEBDIR)/$(VARIANT)/
	$(INSTALLCMD) -m 0755 radiocmd.cgi $(CGIDIR)

	$(INSTALLCMD) -m 0644 $< /etc/systemd/system
	sudo systemctl enable $<
	sudo systemctl start $<

