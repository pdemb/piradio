<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output omit-xml-declaration="yes" method="html" encoding="UTF-8"/>
  <xsl:param name="MAXPOSITIONS" select="10"/>
  <xsl:template match="/rss/channel">
    <h1>
      <xsl:element name="a">
	<xsl:attribute name="href">
	  <xsl:value-of select="link"/>
	</xsl:attribute>
	<xsl:value-of select="title"/>
      </xsl:element>
    </h1>
    <p><xsl:value-of select="description"/></p>
    <dl>
      <xsl:for-each select="item[not(position()>$MAXPOSITIONS)]">
	<dt>
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>/cgi-bin/radiocmd.cgi?url=</xsl:text>
	      <xsl:value-of select="enclosure/@url"/>
	    </xsl:attribute>
	    <xsl:value-of disable-output-escaping="yes" select="title"/>
	  </xsl:element>
	</dt>
	<dd>
	  <em><xsl:value-of select="pubDate"/></em><br/>
	  <span><xsl:value-of disable-output-escaping="yes" select="description"/></span>
	</dd>
      </xsl:for-each>
    </dl>
  </xsl:template>
</xsl:stylesheet>
