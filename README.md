# 𝜫 Radio

Low resource Internet radio using Raspbian on a Pi.

The system uses bare-bone Raspbian commands to facilitate minimal
multimedia playing system.  It works with any Raspberry Pi, esp.
older Pi's, where resources are limited.

## How it works and what it is for

The main part of the package is `radiosrv` shell script that is
started as a systemd service and reads commands from FIFO pseudo-file.
Main purpose of the script is to start and stop omxplayer and that's
how I use it: to control Raspberry Pi playback of podcasts and
Internet radio stations.

Since the IPC mechanism is a simple FIFO, it is possible to
communicate to the script with anything that can write to file.  One
way to control the script is commandline `radiocmd` command, which is
a simple wrapper around `echo` command.

The other way is to control it with CGI script, from a Web browser.


## Dependencies

The most of dependencies are there in a regular Raspbian, although
some of them are not installed by default:

  - bash 
  - systemd
  - omxplayer
  - boa or any other Web server for CGI control
  - xmlstarlet & its deps for podcasts


## Contents

The software installs following files:

  - /etc/systemd/system/radio.service
  - /usr/local/sbin/radiosrv
  - /usr/local/bin/radiocmd
  - /usr/local/share/radio/rssradio.xslt
  - /etc/radio/omx.keys
  - /etc/default/radio
  - /var/www/radio/index.html
  - /var/www/radio/radiopodcast.cgi
  - /usr/local/lib/cgi-bin/radiocmd.cgi


## Installation

A proper deb is under-way, but for now, you need to use Makefile.

Installation is as simple as running `make install`.
It will install the daemon as a system service, which is
started in the main system startup sequence.

To make web interface work, a prerequisite is a working
HTTP server which can handle CGI scripts.  Your configuration
has to allow running CGI scripts from `/usr/local/lib/cgi-bin'
and map it to `/cgi-bin`. The default settings of Boa
web server should work fine, but you need to add this line:

    ScriptAlias /cgi-bin/ /usr/local/lib/cgi-bin/


## Usage

After installation, change file `/etc/default/radio` and set the
RADIOSTREAMADDRESS there to the address of your favorite Internet
radio station.  This address will be used by the daemon.

Then, you may start radio with following command:

    radiocmd start

and stop it with command:

    radiocmd stop

## Usage with more than one radio station

If RADIOSTREAMADDRESS is defined as an array, it is possible to change
the stream using commands 'next' and 'previous'.  For that to work,
the array's 0th element is used to store the current station and all
next elements store all available stations.  So, in order to configure
two radio stations and be able to switch between them, you need to put
similar lines in /etc/default/radio file:

    declare -a RADIOSTREAMADDRESS
    RADIOSTREAMADDRESS[1]=https://my-first-station.com/play
    RADIOSTREAMADDRESS[2]=https://my-second-station.com/mp3
    RADIOSTREAMADDRESS[0]=${RADIOSTREAMADDRESS[1]}

This will set up two stations and make the first station the default
station that will be set after restart or every time after running
command `conf`.

## Podcasts

There's also a 'podcast' command that will fetch and start playing 
the latest podcast from RSS address defined in variable PODCASTADDRESS:

    radiocmd podcast

When used with numeric parameter, that command will play the nth
latest episode of that podcast.  For example, this command will 
play the episode which is prior to the latest episode:

    radiocmd podcast 1

If variable PODCASTADDRESS is an array, you may switch between podcasts 
with command 'prevpodcast' and 'nextpodcast':

    radiocmd prevpodcast

## Other URLs

You can also use `radiosrv` to play any media file that is supported
by omxplayer.  The versatile command `url` will pass any address to
the command.

    radiocmd url=http://your.stream.address/x/y/z

Or, if you want to start it on filesystem:

    radiocmd url=/my/media/dir/file



